package main.java.biz.binarystar.testing.InterviewSample1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import main.java.biz.binarystar.testing.InterviewSample1.ATMTransaction.TransactionType;

/**
* This is an ATMMachine class which is the primary class for operations and management
* of ATM. This class is responsible for all the operations that takes place in an ATM.
* ATMMachine will be used regularly throughout the time and will receive ATMOperations.
* We would need to implement missing pieces of this class to make it useful and fulfill
* operations.
* 
* Accounts HashMap:
* This class has a HashMap of accounts. This HashMap will contain list of account and 
* would use account number as its key.
*
* Ledger TreeMap:
* This class has a TreeMap of ledger. Ledger will contain all running transactions on 
* an account. Every time either debit or credit is used, we need to update ledger
* accordingly.
* 
* @author  BinaryStar
* @version 1.0
* @since   2016-08-15
*/
public class ATMMachine implements ATMOperations, Runnable {
	
	/**
	 * Singleton instance
	 */
	private static ATMMachine _instance = null;
	
	/**
	 * Map to store objects of Accounts based on its account number as key
	 */
	private HashMap<String, Account> accounts = new HashMap<String, Account>();
	
	/**
	 * General ledger for book keeping of all the transactions. Every time a transaction
	 * is used such as debit or credit, we need to update the ledger with transaction details.
	 * The key should always be a unique id which should sort transaction based on time and date.
	 */
	private TreeMap<String, ATMTransaction> ledger = new TreeMap<String, ATMTransaction>();
	
	
	private List<Account> flagList=new LinkedList<>();
	/**
	 * Locked constructor to make sure only one instance should exist
	 */
	private ATMMachine() {
		// Any addition code here
	}
	
	/**
	 * Singleton instance method
	 * @return
	 */
	public static ATMMachine getInstance() {
		if(_instance == null) {
			_instance = new ATMMachine();
		}
		return _instance;
	}
	public double debit(String accountNumber, double amount) {
		// TODO: You need to implement this method in such a way that every time
		// debit happens, the account would be retrieved and amount would be added
		// to that account. You also need to make sure all business rules are met.
		for(Map.Entry<String, Account> entry : accounts.entrySet()){
			if(entry.getKey().equals(accountNumber)){
				Account account=entry.getValue();
				Date date=new Date();
				String transactionType="Debit";
				double openingBalance=account.getBalance();
				double closingBalance=openingBalance+amount;
				ATMTransaction atmt=new ATMTransaction(date,accountNumber,transactionType,openingBalance,closingBalance);
				ledger.put(date.toString(), atmt);
				account.setBalance(closingBalance);	
				accounts.put(accountNumber, account);
			}
		}
		return 0;
	}
/*	public ATMTransaction(Date transactionDate, String accountNumber, String transactionType,
			double openingBalance, double closingBalance) {
		this.transactionDate = transactionDate;
		this.accountNumber = accountNumber;
		if(transactionType.equals("Credit")){
			this.transactionType = TransactionType.Credit;
		}else{
			this.transactionType = TransactionType.Debit;
		}
		this.openingBalance = openingBalance;
		this.closingBalance = closingBalance;
	}*/

	public double credit(String accountNumber, double amount) {
		// TODO: You need to implement this method in such a way that every time
		// credit happens, the account would be retrieved and amount would be deducted
		// from that account. You also need to make sure all business rules are met.
		for(Map.Entry<String, Account> entry : accounts.entrySet()){
			if(entry.getKey().equals(accountNumber)){
				Account account=entry.getValue();
				Date date=new Date();
				String transactionType="Credit";
				double openingBalance=account.getBalance();
				double closingBalance=openingBalance-amount;
				ATMTransaction atmt=new ATMTransaction(date,accountNumber,transactionType,openingBalance,closingBalance);
				ledger.put(date.toString(), atmt);
				account.setBalance(closingBalance);	
				accounts.put(accountNumber, account);
				if(closingBalance<500){
					flagList.add(account);
				}
			}
		}
		return 0;
	}

	public double getBalance(String accountNumber) {
		// TODO: You need to implement this method in such a way that every time
		// check balance happens, the account would be retrieved and amount would be 
		// retrieved from that account and returned back. 
		// You also need to make sure all business rules are met.
	
		return accounts.get(accountNumber).getBalance();
	}

	public String openAccount(String accountType, double openingBalance){
		// TODO: You need to implement this method in such a way that every time
		// open account is called, an account should be opened with provided details
		// You also need to make sure all business rules are met.
			Account account=new Account("",accountType,openingBalance);
			accounts.put(account.getAccountNumber(), account);
			/*System.out.print("open"+account.getAccountNumber());*/
			return account.getAccountNumber();
		
	}

	public boolean closeAccount(String accountNumber) {
		// TODO: You need to implement this method in such a way that every time
		// close account is called, an account should be closed with provided account id
		// You also need to make sure all business rules are met.
		if(accounts.remove(accountNumber)!=null){
			return true;
		}
		return false;
	}

	/**
	 * @return the accounts
	 */
	public HashMap<String, Account> getAccounts() {
		return accounts;
	}

	/**
	 * @param accounts the accounts to set
	 */
	public void setAccounts(HashMap<String, Account> accounts) {
		this.accounts = accounts;
	}

	/**
	 * @return the ledger
	 */
	public TreeMap<String, ATMTransaction> getLedger() {
		return ledger;
	}

	/**
	 * @param ledger the ledger to set
	 */
	public void setLedger(TreeMap<String, ATMTransaction> ledger) {
		this.ledger = ledger;
	}

	/**
	 * BONUS: This is the second part of the quiz. You need to make ATMMachine such that
	 * it can be run on a separate thread every time it is called.
	 */
	public void run() {
		// TODO: Implement the run method to take multiple operations
		// Load transactions.txt file and run transactions. Transactions
		// without parenthesis '()' means they are debit transaction
		// transaction with parenthesis means they are credit. The line starting
		// with star means it is account number.
		String fileName = "transactions.txt";
		BufferedReader br = null;
		FileReader fr = null;

		try {

			fr = new FileReader(fileName);
			br = new BufferedReader(fr);
			String accountNumber="";
			double balance=0;
			String sCurrentLine;

			br = new BufferedReader(new FileReader(fileName));

			while ((sCurrentLine = br.readLine()) != null) {
			if(sCurrentLine.charAt(0)=='*'){
				
				accountNumber=sCurrentLine.substring(1);
				System.out.println(accountNumber);
				
			}else if(sCurrentLine.charAt(0)=='('){
				balance-=Double.parseDouble(sCurrentLine.substring(1, sCurrentLine.length()-1));
				System.out.println(sCurrentLine.substring(1, sCurrentLine.length()-1));
			}else{
				balance+=Double.parseDouble(sCurrentLine);
				System.out.println(sCurrentLine);
			}
			}
			Account account=new Account(accountNumber,"Checking",balance);
			accounts.put(accountNumber, account);

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (br != null)
					br.close();

				if (fr != null)
					fr.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}

		}
		
	}

	public boolean isFlagged(String accountNumber) {
		// TODO: Your task is to make sure if account balance goes below $500 than 
		// account should be flagged
		if(getBalance(accountNumber)<500){
			return true;
		}
		return false;
	}
	

}
