package main.java.biz.binarystar.testing.InterviewSample1;

public class NotEnoughBalanceException extends RuntimeException{
	public NotEnoughBalanceException(String message) {
        super(message);
    }
}
