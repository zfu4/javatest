package main.java.biz.binarystar.testing.InterviewSample1;

import java.util.UUID;

/**
* This is an Account class which is used to keep track of activities within
* a particular account. Account class should have an opening balance and it 
* cannot start from $0. The minimum balance an account should contain is 
* $500.
*
* @author  BinaryStar
* @version 1.0
* @since   2016-08-15
*/
public class Account {
	
	
	/**
	 * Variable to contain running balance of an account
	 */
	private double balance;
	
	/**
	 * Variable to contain Account Number. This would be a unique string
	 */
	private String accountNumber;
	
	/**
	 * Flag to identify if account is deactivated or not
	 */
	private boolean deactivated;
	
	/**
	 * Enum to identify the Account Type
	 */
	private AccountType accountType;

	// TODO: Define a constructor that should take an opening balance.
	// If an opening balance is less than $500 class should throw an
	// exception and should not create the object.
	
	// If balance is below $500 than account should be flagged and should
	// be included in flagged list contained by ATM machine.
	public Account(String accountNumber,String accountType,double balance){
			if(accountNumber.length()==0||accountNumber==null){
				this.accountNumber=UUID.randomUUID().toString();
			}else{
				this.accountNumber=accountNumber;
			}
			this.deactivated=false;
			
			System.out.println(accountNumber);
			if(accountType.equals("Checking")){
				this.accountType=AccountType.Checking;
			}else if(accountType.equals("Savings")){
				this.accountType=AccountType.Savings;
			}else{
				this.accountType=AccountType.Credit;
			}
			if(balance<500){
				throw new NotEnoughBalanceException("The balance is less than 500");
			}else{
			this.balance=balance;
			}
	}
	/**
	 * Account Type can be either Checking, Savings or Credit
	 * @author BinaryStar
	 *
	 */
	public enum AccountType {Checking, Savings, Credit}
	
	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the deactivated
	 */
	public boolean isDeactivated() {
		return deactivated;
	}

	/**
	 * @param deactivated the deactivated to set
	 */
	public void setDeactivated(boolean deactivated) {
		this.deactivated = deactivated;
	}

	/**
	 * @return the balance
	 */
	public double getBalance() {
		return balance;
	}

	/**
	 * TODO: Make sure balance is greater than $500 otherwise flag the account.
	 * @param balance the balance to set
	 */
	public void setBalance(double balance) {
		this.balance = balance;
	}

	/**
	 * @return the accountType
	 */
	public AccountType getAccountType() {
		return accountType;
	}

	/**
	 * @param accountType the accountType to set
	 */
	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

}
